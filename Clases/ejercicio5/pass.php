<?php  
//declaracion de clase token
	class password{
		//declaracion de atributos
		private $nombre;
		private $password;
		//declaracion de metodo constructor
		public function __construct($nombre_front){
			$this->nombre=$nombre_front;
            // Aquí llamamos a la función que nos generará una contraseña aleatoria
			$this->password=$this->generar_password();
		}
        
        // Creamos un método privado que solo lo utilizará la clase
        // Toma una longitud y genera una cadena aleatoria de esa 
        // longitud con los caracteres especificados
        private function generar_password($longitud = 4) {
            $caracteres = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
            $lon_caracteres = strlen($caracteres);
            $password = '';
            for ($i = 0; $i < $longitud; $i++) {
                $password .= $caracteres[rand(0, $lon_caracteres - 1)];
            }
            return $password;
        }
        
        // Método para mostrar la contraseña
        public function mostrar(){
			return 'Hola '.$this->nombre.' esta es tu contraseña: '.$this->password;
		}

		//declaracion de metodo destructor
		public function __destruct(){
			// se muestra en pantalla
			echo "Tu contraseña fue:".$this->password."\t";
		}
	}

$pass_mensaje='';

if (!empty($_POST)){
	//creacion de objeto de la clase
	$pass1= new password($_POST['nombre']);
	$pass_mensaje=$pass1->mostrar();
}

?>
