<?php

//crea aqui la clase Moto junto con dos propiedades public
class Moto{
	public $color;
	public $placa;
}
//crea aqui la instancia o el objeto de la clase Moto
$mi_moto = new Moto();

$mensajeServidorMoto = "";
if ( !empty($_POST)){

 	// recibe aqui los valores mandados por post y arma el mensaje para front 
	$mi_moto->color=$_POST['color_moto'];
	$mi_moto->placa=$_POST['placa_moto'];

	$mensajeServidorMoto .= 'Color de la moto '.$mi_moto->color;
	$mensajeServidorMoto .= '  Placa de la moto '.$mi_moto->placa;
}  


?>
