<?php  
include_once('transporte.php');

	class tanque extends transporte{
		private $calibre;

		//sobreescritura de constructor
		public function __construct($nom,$vel,$com,$cal){
			parent::__construct($nom,$vel,$com);
			$this->calibre=$cal;
		}

		// sobreescritura de metodo
		public function resumenTanque(){
			$mensaje=parent::crear_ficha();
			$mensaje.='<tr>
						<td>Calibre:</td>
						<td>'. $this->calibre.'</td>				
					</tr>';
			return $mensaje;
		}
	}

?>
