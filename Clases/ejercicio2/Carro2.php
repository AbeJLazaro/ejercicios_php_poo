<?php
//creación de la clase carro
class Carro2{
	//declaracion de propiedades
	public $color;
	public $modelo;
	private $circula;

	//declaracion del método verificación
	public function verificacion($anio){
		if($anio<1990){
			$this->circula = "no";
		}elseif($anio>=1990 && $anio<=2010){
			$this->circula = "revision";
		}else{
			$this->circula = "si";
		}
	}
	//getter del atributo privado
	public function get_circula(){
		return $this->circula;
	}
}

//creación de instancia a la clase Carro
$Carro1 = new Carro2();

//asignación de atributos y llamada a método
if (!empty($_POST)){
	$Carro1->color=$_POST['color'];
	$Carro1->modelo=$_POST['modelo'];
	$Carro1->verificacion($_POST['anio']);
}




